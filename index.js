let container = document.getElementById('block');
let name = document.getElementById('name');

function getState() {
    let cc = document.forms['myForm']['input'].value;
    getCity(cc);
}

const getCity = async cc => {
    const url = 'https://api.openweathermap.org/data/2.5/weather?q=' + cc + '&appid=c471954c86ec05d2ae26de9d4848a404';
    const res = await fetch(url);
    const city = await res.json();
    container.innerHTML = '<img src="https://www.countryflags.io/' + city.sys.country + '/flat/64.png" alt="">';
};